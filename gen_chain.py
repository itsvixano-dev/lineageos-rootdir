#!/usr/bin/env python3

import argparse
import json
import re
import xml.etree.ElementTree as ET
from pathlib import Path

import requests


def load_manifest_paths():
    paths = {}
    manifest_urls = [
        'https://raw.githubusercontent.com/LineageOS/android/lineage-22.1/default.xml',
        'https://raw.githubusercontent.com/LineageOS/android/lineage-22.1/snippets/lineage.xml',
        'https://raw.githubusercontent.com/LineageOS/android/lineage-22.1/snippets/pixel.xml',
    ]

    for url in manifest_urls:
        response = requests.get(url)
        root = ET.fromstring(response.text)
        for project in root.findall('.//project'):
            name = project.get('name')
            path = project.get('path', name)
            revision = project.get('revision', 'master')
            if name not in paths:
                paths[name] = []
            paths[name].append({'path': path, 'revision': revision})
    return paths


def get_changes(url: str, topic: str) -> dict:
    response = requests.get(
        f'{url}/changes/?q=topic:{topic}&o=CURRENT_REVISION&o=CURRENT_COMMIT'
    )
    return json.loads(response.text.removeprefix(")]}'"))


def get_change_ref(change_id: str) -> tuple:
    match = re.search(r'(\d+)/(\d+)', change_id)
    if match:
        change_num = match.group(2)
        last_digits = change_num[-2:]
        return f'refs/changes/{last_digits}/{change_num}'
    return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('topic')
    parser.add_argument('--gerrit-url', default='https://review.lineageos.org')
    args = parser.parse_args()

    try:
        project_paths = load_manifest_paths()
        changes = get_changes(args.gerrit_url, args.topic)
        if not changes:
            return print('No changes found')

        active_changes = [
            c
            for c in sorted(changes, key=lambda x: x['_number'], reverse=True)
            if c['status'] not in ('MERGED', 'ABANDONED')
        ]

        if not active_changes:
            return print('No active changes found')

        projects = {}
        for c in active_changes:
            if (
                c['project'] not in projects
                or c['_number'] > projects[c['project']]['_number']
            ):
                projects[c['project']] = c

        script = [
            '#!/bin/bash',
            '',
            '_TOP=$(pwd)',
            '',
            'set -e',
            '',
        ]

        for change in projects.values():
            project_name = change['project']
            project_info_list = project_paths.get(
                project_name, [{'path': project_name, 'revision': 'master'}]
            )
            current_revision = change['current_revision']
            ref = change['revisions'][current_revision]['ref']
            branch = change['branch']
            project_info = next(
                (
                    info
                    for info in project_info_list
                    if info['revision'] == branch
                ),
                project_info_list[0],
            )
            project_path = project_info['path']

            script.extend(
                [
                    f'# {project_path}',
                    f'if [ -d {project_path} ]; then',
                    f'    cd {project_path}',
                    f'    git fetch https://github.com/{project_name} {ref} && git reset --hard FETCH_HEAD # {change.get("subject", "")}',
                    '    cd ${_TOP}',
                    'else',
                    f'    echo "Error: {project_path} directory not found"',
                    '    exit 1',
                    'fi',
                    '',
                ]
            )

        script.append('wait')

        output = Path('gen_picks')
        output.write_text('\n'.join(script))
        output.chmod(0o755)
        print('Generated gen_picks')

    except Exception as e:
        print(f'Error: {e}')
        return 1


if __name__ == '__main__':
    exit(main())
